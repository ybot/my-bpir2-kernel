#!/bin/bash
# references
#http://fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:storage#sd-card
#http://fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:ubuntu

set -Eeuo pipefail

# vars
: ${MY_RESULT_DIR:=${PWD}/build-$(date +%s)}
: ${MY_ROOTIMG_FILE:=${MY_RESULT_DIR}/bpir2.img}
: ${MY_ROOTIMG_SIZE:=1300M}
: ${MY_ROOTIMG_SIZE_BOOT:=200}  ## /boot size in MB
: ${MY_HOSTNAME:=localhost}
: ${MY_FLAVOUR:=debian}
: ${MY_RELEASE:=bullseye}
: ${MY_INCLUDES_MITICO:="apparmor my-zerotouch my-bpir2-wifi my-bpir2-kernel my-bpir2-uboot"}	## *space* separated list of packages to be installed off the mitico repo, if empty mitico repo won't be added at all
: ${MY_INCLUDES:="init,udev,sudo,ifupdown,iproute2,isc-dhcp-client,iputils-ping,openssh-server,vim,ncurses-term,less,netbase,kmod"}
: ${MY_EXCLUDES:="rsyslog"}

: ${MY_ROOT_PASSWD:='$6$65lpe41.cnKY$YF.Y.rFmeduuInQjGKAUxagNY/Z1vKHo4cUXOIpHeCFYmDvigQdZ0KCigs8yB7/nxI1MhKA546N4w1JPxTcbQ.'}    ## passwd: firstpass
: ${MY_ROOT_KEY:='ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMCUvBdM5PR+qdy3LahmeeD++6G9mkOT8ZqmHMKd1eOL'}

: ${TMP_DIR:=$(mktemp -d -t bpir2-XXXXXX)}

if [ "$UID" -ne 0 ]; then
	echo "### sorry, need to be root to do all this.... ###"
	exit 1
fi

echo "#########################################################################"
echo "#########################################################################"
echo "###                                                                   ###"
echo "###   WARN: you'll need about 25-30 gigs of disk space for all this   ###"
echo "###   (if no deps are yet installed)                                  ###"
echo "###                                                                   ###"
echo "#########################################################################"
echo "#########################################################################"

[ -e "${MY_ROOTIMG_FILE}" ] && echo "###  tmp working file: ${MY_ROOTIMG_FILE} already exists, going to remove and start fresh...  ###" && rm ${MY_ROOTIMG_FILE}
[ -e "${MY_ROOTIMG_FILE}.gz" ] && echo "###  image file already exists, going to remove it and start fresh, ok?  ###" && rm -i ${MY_ROOTIMG_FILE}.gz


if [ ${MY_FLAVOUR} == "debian" ]; then
    : ${MY_MIRROR:="http://ftp.us.debian.org/debian"}
    : ${MY_MIRROR_SEC:="http://security.debian.org/debian-security"}
    : ${MY_COMPONENTS:="main,contrib,non-free"}
elif [ ${MY_FLAVOUR} == "ubuntu" ]; then
    : ${MY_MIRROR:="http://archive.ubuntu.com/ubuntu"}
    : ${MY_MIRROR_SEC:="http://security.ubuntu.com/ubuntu"}
    : ${MY_COMPONENTS:="main,restricted,multiverse,universe"}
else
    echo "unsupported flavour" && exit 10
fi

# cause for some reason debian stable can't be consistent....
if [ "${MY_RELEASE}" == "buster" ] || [ "${MY_RELEASE}" == "stable" ]; then
    : ${MY_DISTRO_SEC:="${MY_RELEASE}/updates"}
fi

: ${MY_DISTROS:="${MY_RELEASE} ${MY_RELEASE}-updates ${MY_RELEASE}-backports"}
: ${MY_DISTRO_SEC:="${MY_RELEASE}-security"}


# get deps, prob needs more
apt-get update
apt-get install qemu-user-static debootstrap binfmt-support curl fdisk dosfstools


# sd card image
[ ! -d "${MY_RESULT_DIR}" ] && mkdir -p "${MY_RESULT_DIR}"
fallocate -l ${MY_ROOTIMG_SIZE} ${MY_ROOTIMG_FILE}

BOOTSECS=$((${MY_ROOTIMG_SIZE_BOOT}*1024*1024/512))
ROOTSECSST=$((${BOOTSECS}+204800))
#first 100 MB for uboot and other binary blops
#start /boot partition at sec 204800, the rest really doesn't matter.
sfdisk ${MY_ROOTIMG_FILE} <<-EOF
204800,${BOOTSECS},c
${ROOTSECSST},,83
EOF

#we should try GPT and cleaner partitioning. low priority but this is how it would look like
#sgdisk --clear --new 1:204800:+256M --typecode=1:0700 --change-name=1:'BPI-BOOT' --new 2::-0 --typecode=2:8300 --change-name=2:'BPI-ROOT' ${dev}

dev=$(losetup --find --show --partscan ${MY_ROOTIMG_FILE})
dev_boot=${dev}p1
dev_root=${dev}p2

mkfs -t vfat -n BPI-BOOT -F 32 ${dev_boot}
mkfs -t ext4 -L BPI-ROOT ${dev_root}

uuid_boot=$(lsblk -no UUID ${dev_boot})
uuid_root=$(lsblk -no UUID ${dev_root})
partuuid_root=$(lsblk -no PARTUUID ${dev_root})

targetdir=${TMP_DIR}

chown root.root ${targetdir}		## chroot needs to be owned by root.root otherwise deboot fails
mount ${dev_root} ${targetdir}
mkdir -p ${targetdir}/boot
mount ${dev_boot} ${targetdir}/boot


# debootstrap root fs
debootstrap --variant=minbase --arch=armhf --foreign --components=${MY_COMPONENTS} --include=${MY_INCLUDES} --exclude=${MY_EXCLUDES} ${MY_RELEASE} ${targetdir} ${MY_MIRROR}
cp -i /usr/bin/qemu-arm-static ${targetdir}/usr/bin/
LANG=C chroot ${targetdir} /debootstrap/debootstrap --second-stage

# set root login
chroot ${targetdir} usermod -p "${MY_ROOT_PASSWD}" root
mkdir -p ${targetdir}/root/.ssh
echo "${MY_ROOT_KEY}" | tee ${targetdir}/root/.ssh/authorized_keys
chmod 700 ${targetdir}/root/.ssh
chmod 600 ${targetdir}/root/.ssh/authorized_keys


# fixup various other rootfs needs
tee ${targetdir}/etc/fstab <<-EOF
	proc                                           /proc           proc    defaults                      0   0
	UUID=${uuid_root}      /               ext4    errors=remount-ro,noatime     0   1
	UUID=${uuid_boot}                                 /boot           vfat    umask=0077                    0   2
EOF

tee ${targetdir}/etc/hostname <<-EOF
	${MY_HOSTNAME}
EOF

tee ${targetdir}/etc/hosts <<-EOF
	127.0.0.1      localhost
	::1            localhost       ip6-localhost     ip6-loopback
	ff02::1        ip6-allnodes
	ff02::2        ip6-allrouters
EOF

tee ${targetdir}/etc/resolv.conf <<-EOF
	nameserver 2606:4700:4700::1111
	nameserver 1.1.1.1
EOF

# network interface template
tee ${targetdir}/etc/network/interfaces <<-EOF
	auto lo
	iface lo inet loopback

	auto eth0
	iface eth0 inet6 manual

	auto wan
	iface wan inet6 auto
EOF

# banner
[ ! -d "${targetdir}/etc/issue.d" ] && mkdir "${targetdir}/etc/issue.d"
tee ${targetdir}/etc/issue.d/show-ips.issue <<-EOF
	ipv4: \4   ipv6: \6
EOF

# repo sources
rm -f ${targetdir}/etc/apt/sources.list
for d in ${MY_DISTROS}; do
	echo "deb ${MY_MIRROR}/ ${d} ${MY_COMPONENTS//,/ }" | tee -a ${targetdir}/etc/apt/sources.list
	echo "deb-src ${MY_MIRROR}/ ${d} ${MY_COMPONENTS//,/ }" | tee -a ${targetdir}/etc/apt/sources.list
done
echo "deb ${MY_MIRROR_SEC}/ ${MY_DISTRO_SEC} ${MY_COMPONENTS//,/ }" | tee -a ${targetdir}/etc/apt/sources.list
echo "deb-src ${MY_MIRROR_SEC}/ ${MY_DISTRO_SEC} ${MY_COMPONENTS//,/ }" | tee -a ${targetdir}/etc/apt/sources.list


## add mitico repo if asked for
if [ -n "${MY_INCLUDES_MITICO}" ]; then
	curl --fail -Lo ${targetdir}/etc/apt/trusted.gpg.d/mitico-archive.gpg http://repo.mitico.us/public.gpg
	echo "deb http://repo.mitico.us mitico main" | tee ${targetdir}/etc/apt/sources.list.d/mitico.list
	LC_ALL=C DEBIAN_FRONTEND=noninteractive chroot ${targetdir}/ apt-get update
	LC_ALL=C DEBIAN_FRONTEND=noninteractive chroot ${targetdir}/ apt-get -y install ${MY_INCLUDES_MITICO//,/ }
	LC_ALL=C DEBIAN_FRONTEND=noninteractive chroot ${targetdir}/ apt-get clean
	chroot ${targetdir} systemctl disable hostapd
	my_kernel=$(chroot ${targetdir} dpkg -l | grep linux-image | awk '{ print $2 }')
	chroot ${targetdir} dpkg-reconfigure ${my_kernel}
	rm -rf ${targetdir}/var/lib/apt
else
	## add helper script to add mitico repo easily if one so wishes
	tee ${targetdir}/root/add-mitico-repo.sh <<-EOF
		#!/bin/bash
		set -Eeuo pipefail
		apt-get install curl
		curl --fail -Lo /etc/apt/trusted.gpg.d/mitico-archive.gpg http://repo.mitico.us/public.gpg
		echo "deb http://repo.mitico.us mitico main" | tee /etc/apt/sources.list.d/mitico.list
		apt-get update
		apt-get install my-common-config
	EOF
	chmod 755 ${targetdir}/root/add-mitico-repo.sh
fi

## make it a golden image
rm -f ${targetdir}/etc/ssh/ssh_host_* ${targetdir}/etc/machine-id ${targetdir}/var/lib/dbus/machine-id

## good for flash based installs
tee -a ${targetdir}/etc/systemd/journald.conf <<-EOF
	## mygw addons/tweaks - helpful for flash based media - once all stable consider uncommenting MaxLevelStore
	SyncIntervalSec=60m
	#MaxLevelStore=err
EOF


## BPI-r2 specific

for blob in BPI-R2-HEAD440-0k.img.gz BPI-R2-HEAD1-512b.img.gz BPI-R2-preloader-DDR1600-20191024-2k.img.gz; do
	[ ! -e "${blob}" ] && curl --fail -Lo /tmp/${blob} "https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/${blob}"
done

# binary blops clone repo: (to be verified, just recollected from memory. probably not right)
zcat /tmp/BPI-R2-HEAD440-0k.img.gz | dd of=${MY_ROOTIMG_FILE} bs=1024 seek=0 conv=notrunc
zcat /tmp/BPI-R2-HEAD1-512b.img.gz | dd of=${MY_ROOTIMG_FILE} bs=512 seek=1 conv=notrunc
zcat /tmp/BPI-R2-preloader-DDR1600-20191024-2k.img.gz | dd of=${MY_ROOTIMG_FILE} bs=1024 seek=2 conv=notrunc

: ${MY_UBOOT_BIN:=$(ls -t ${targetdir}/lib/u-boot/u-boot-*.bin)}
dd if=${MY_UBOOT_BIN} of=${MY_ROOTIMG_FILE} bs=1k seek=320 conv=notrunc


# default kernel cmdline and u-boot environment
[ ! -d "${targetdir}/boot/bananapi/bpi-r2/linux" ] && mkdir -p "${targetdir}/boot/bananapi/bpi-r2/linux"
my_kernel=$(ls -t ${targetdir}/boot/bananapi/bpi-r2/linux/uImage-*)
tee -a ${targetdir}/boot/bananapi/bpi-r2/linux/uEnv.txt <<-EOF
	root=PARTUUID=${partuuid_root} rootfstype=ext4 rootwait cfg80211.ieee80211_regdom=US fsck.repair=yes clocksource=timer
	#for better debug and/or make tty1 vs ttyS0 the primary console (kernel will honor both, but systemd will only use the console defined last)
	#console=earlyprintk console=tty1 console=ttyS0,115200 systemd.log_level=debug systemd.debug-shell=1
	kernel=${my_kernel##*/}
EOF

# u-boot env location config file. helpful for troubleshooting
tee ${targetdir}/etc/fw_env.config <<-EOF
	# change this to mmcblk0 if booting from emmc
	/dev/mmcblk1 0x100000 0x2000
EOF

# blacklist powerbtn module to prevent boot loop when powerbtn is soldered closed (to be recommanded if you want this thing to auto-power on....)
tee ${targetdir}/etc/modprobe.d/blacklist-powerbtn.conf <<-EOF
	blacklist mtk_pmic_keys
EOF
ln -s ../modprobe.d/blacklist-powerbtn.conf ${targetdir}/etc/modules-load.d/blacklist-powerbtn.conf




# cleanup
rm ${targetdir}/usr/bin/qemu-arm-static
df -h ${targetdir}/boot
umount ${targetdir}/boot
df -h ${targetdir}
umount ${targetdir}
rm -rf ${targetdir}
losetup -d ${dev}
gzip ${MY_ROOTIMG_FILE}
