#!/bin/bash
set -Eexuo pipefail
DATE="$(date +%s)"

: ${MY_KERNEL_REPO:="https://github.com/frank-w/BPI-Router-Linux.git"}
: ${MY_KERNEL_BRANCH:=5.15-main}
: ${MY_KERNEL_CONFIG:=${PWD}/configs/kernel-5.15.config}
: ${MY_KERNEL_PATH:="BPI-Router-Linux"}
#: ${MY_KERNEL_PATCHES:=${PWD}/patches/*.patch}

: ${MY_LOCALVERSION="-${DATE}"}
: ${MY_RESULT_DIR:="${PWD}/debbuilder-${DATE}"}
: ${MY_BUILDDIR:="/tmp/my-bpir2-kernel-build"}

function backup_config {
	[ ! -d "${MY_RESULT_DIR}" ] && mkdir "${MY_RESULT_DIR}"
	if ! diff -q ${MY_KERNEL_CONFIG} .config; then
		echo "##"
		echo "##  kernel config changed from original, copying modified config along the builds"
		echo "##"
		cp -v .config "${MY_RESULT_DIR}/config-${MY_KERNEL_BRANCH}"
	fi
}

function find_dtb() {
	for i in arch/arm/boot/dts/*${board}.dtb arch/arm/boot/dts/mediatek/*${board}.dtb; do
		if [ -e "${i}" ]; then
			echo "##  Copying ${i} to ./${board}.dtb"
			# need to copy that cause of broken scripts/package/builddeb
			cp "${i}" ./${board}.dtb
			md5sum ./${board}.dtb
			return 0
		fi
	done

	echo "##"
	echo "##  failed to find DTB"
	echo "##"
	return 1
}

[ ! -d "${MY_BUILDDIR}" ] && mkdir -p "${MY_BUILDDIR}"
cd "${MY_BUILDDIR}"
[ ! -d "${MY_KERNEL_PATH}" ] && git clone -b ${MY_KERNEL_BRANCH} ${MY_KERNEL_REPO} "${MY_KERNEL_PATH}"

cp ${MY_KERNEL_CONFIG} "${MY_KERNEL_PATH}/.config"

cd "${MY_KERNEL_PATH}"

if [ -n "${MY_KERNEL_PATCHES:-}" ]; then
	git apply --ignore-whitespace ${MY_KERNEL_PATCHES}
fi

#./build.sh build
#./build.sh pack_debs
#export DPKG_FLAGS="--sign-key='$(git config --get user.email)' --force-sign"
#export DEBFULLNAME="$(git config --get user.name)"
#export board="bpi-r2"
export KBUILD_BUILD_HOST=docker
export LOCALVERSION="-${MY_KERNEL_BRANCH#*-}${MY_LOCALVERSION}"
export ARCH=arm
export CROSS_COMPILE='ccache arm-linux-gnueabihf-'
export board=bpi-r2

kernver="$(make kernelversion)"
CFLAGS="-j$(grep ^processor /proc/cpuinfo  | wc -l)"

if [ "${1:-}" == "config" ]; then
	make menuconfig
	cp -iv .config "${MY_RESULT_DIR}/config-menuconfig"
	#echo exiting after menuconfig
	#exit 0
fi

trap backup_config ERR

## some reason the builddeb script included keeps changing. right now it expects the dtb file to be in the root. it also expects us to generate the fti
## which fine by us, so we don't have to do it upon installing the kernel and save a bit of space on /boot
## needs work here needs to match a bit with what we do in build-kernel
find_dtb

# image with dtb
#make ${CFLAGS}
#cat arch/arm/boot/zImage ./bpi-r2.dtb >arch/arm/boot/zImage-dtb
#mkimage -A arm -O linux -T kernel -C none -a 80008000 -e 80008000 -n "Linux Kernel ${kernver}${LOCALVERSION}" -d arch/arm/boot/zImage-dtb "./uImage"

# image without dtb
DTC_FLAGS=-@ make ${CFLAGS} CONFIG_ARM_APPENDED_DTB=n
mkimage -A arm -O linux -T kernel -C none -a 80008000 -e 80008000 -n "Linux Kernel ${kernver}${LOCALVERSION}" -d arch/arm/boot/zImage "./uImage_nodt"

# build deb
make bindeb-pkg

backup_config
mv -v ../linux-* "${MY_RESULT_DIR}"
